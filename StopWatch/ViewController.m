//
//  ViewController.m
//  StopWatch
//
//  Created by MattiasO on 2015-03-18.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;
@property (nonatomic) NSTimer *timer;
@property int timeSec;
@property int timeMin;
@property int startPress;

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.label.text = @"0";
    self.timeSec = 0;
    self.timeMin = 0;
}

- (IBAction)startButtonPressed:(id)sender {

    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(tickTack) userInfo:nil repeats:YES];
    
    if ([self.startButton.titleLabel.text isEqualToString:@"START"]) {
        [self.startButton setTitle:@"PAUSE" forState:UIControlStateNormal];
    } else {
        [self.timer invalidate];
        [self.startButton setTitle:@"START" forState:UIControlStateNormal];
    }

}

- (IBAction)resetButtonPressed:(id)sender {
    [self.timer invalidate];
    self.label.text = @"0";
    self.timeMin = 0;
    self.timeSec = 0;
    [self.startButton setTitle:@"START" forState:UIControlStateNormal];
}

-(void)tickTack {
    self.timeSec++;
    if(self.timeSec > 59) {
        self.timeSec = 0;
        self.timeMin++;
    }
    
    self.label.text = [NSString stringWithFormat:@"%d:%d", self.timeMin, self.timeSec];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
